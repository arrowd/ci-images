FROM fedora:27

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ENV LANG C.UTF-8

# Core build utilities
RUN dnf -y install coreutils binutils which git make \
    automake autoconf gcc perl python3 texinfo xz pxz bzip2 lbzip2 \
    patch openssh-clients sudo zlib-devel sqlite \
    ncurses-compat-libs gmp-devel ncurses-devel gcc-c++ findutils \
    curl wget jq

# Documentation tools
RUN dnf -y install python3-sphinx \
    texlive texlive-latex texlive-xetex \
    texlive-collection-latex texlive-collection-latexrecommended \
    texlive-xetex-def texlive-collection-xetex \
    python-sphinx-latex dejavu-sans-fonts dejavu-serif-fonts \
    dejavu-sans-mono-fonts

# This is in the PATH when I ssh into the CircleCI machine but somehow
# sphinx-build isn't found during configure unless we explicitly
# add it here as well; perhaps PATH is being overridden by CircleCI's
# infrastructure?
ENV PATH /usr/libexec/python3-sphinx:$PATH

# systemd isn't running so remove it from nsswitch.conf
# Failing to do this will result in testsuite failures due to
# non-functional user lookup (#15230).
RUN sed -i -e 's/systemd//g' /etc/nsswitch.conf

WORKDIR /tmp
# Install GHC and cabal
ENV GHC_VERSION 8.8.3
RUN curl -L https://downloads.haskell.org/~ghc/$GHC_VERSION/ghc-$GHC_VERSION-x86_64-fedora27-linux.tar.xz | tar -Jx;
WORKDIR /tmp/ghc-$GHC_VERSION
RUN ./configure --prefix=/opt/ghc/$GHC_VERSION; \
    make install; \
    rm -rf /tmp/ghc-$GHC_VERSION
ENV PATH /opt/ghc/$GHC_VERSION/bin:$PATH

WORKDIR /tmp
# Get Cabal
ENV CABAL_VERSION 3.0.0.0
RUN curl -L https://downloads.haskell.org/cabal/cabal-install-$CABAL_VERSION/cabal-install-$CABAL_VERSION-x86_64-unknown-linux.tar.xz | tar -Jx && \
    mv cabal /usr/local/bin/cabal

# Create a normal user.
RUN adduser ghc --comment "GHC builds"
RUN echo "ghc ALL = NOPASSWD : ALL" > /etc/sudoers.d/ghc
USER ghc
WORKDIR /home/ghc/

# Build Haskell tools
RUN cabal v2-update && \
    cabal v2-install hscolour happy alex --constraint 'happy ^>= 1.19.10'
ENV PATH /home/ghc/.cabal/bin:$PATH

CMD ["bash"]
